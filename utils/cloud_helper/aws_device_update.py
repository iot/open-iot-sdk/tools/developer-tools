#!/usr/bin/env python3

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

import click
import os
import time
from pathlib import Path

import boto3

# AWS Resources prefixes
PREFIX_IOT_THING_NAME = "iotmsw_ci-"
PREFIX_S3_BUCKET = "iotmsw-ci-bucket-"
PREFIX_OTA_POLICY = "iotmsw-ci-policy-"
PREFIX_OTA_UPDATE_NAME = "ota-test-update-id-"
PREFIX_OTA_UPDATE_JOB_NAME = "AFR_OTA-"

# The following environment variables are predefined in the CI environment and
# same across all pipelines.
AWS_REGION = os.environ["AWS_REGION"]
OTA_ROLE_NAME = os.environ["IOT_OTA_ROLE_NAME"]
OTA_CERTIFICATE_ID = os.environ["IOT_OTA_CERT_ID"]

AWS_ACCOUNT = boto3.client("sts").get_caller_identity().get("Account")
OTA_POLICY = f"arn:aws:iot:{AWS_REGION}:{AWS_ACCOUNT}:cert/{OTA_CERTIFICATE_ID}"
OTA_ROLE_ARN = f"arn:aws:iam::{AWS_ACCOUNT}:role/{OTA_ROLE_NAME}"

# AWS Clients
client_iot = boto3.client("iot", AWS_REGION)
client_s3 = boto3.client("s3", AWS_REGION)


@click.group()
def main() -> None:
    """Main CLI entry point."""
    pass


def get_file_content(path: str, mode="r") -> str:
    """
    Get the content of a file.

    Args:
        path (str): The path to the file.
        mode (str, optional): The mode to open the file (default is "r" for read).

    Returns:
        str: The content of the file.
    """
    with open(path, mode) as fp:
        return fp.read()


def wait_for_ota(id: str, action: str) -> None:
    """
    Wait for the completion of an OTA update.

    Args:
        id (str): The OTA update ID.
        action (str): The action to wait for ("create" or "delete").

    Raises:
        Exception: If the OTA update fails or times out.
    """
    STATUS_SUCCESS = f"{action.upper()}_COMPLETE"
    STATUS_FAILURE = f"{action.upper()}_FAILED"
    while True:
        ota_update = None
        try:
            ota_update = client_iot.get_ota_update(otaUpdateId=id)
        except Exception as e:
            raise Exception(f"Failed to get OTA update with ID `{id}`: {e}")
        else:
            ota_update_status = ota_update["otaUpdateInfo"]["otaUpdateStatus"]
            if ota_update_status == STATUS_SUCCESS:
                print(f"Update with ID `{id}` {action} successful")
                break
            elif ota_update_status == STATUS_FAILURE:
                ota_update_status_message = ota_update["otaUpdateInfo"]["errorInfo"][
                    "message"
                ]
                raise Exception(
                    f"Failed to {action} OTA update with ID {id}: "
                    f"{ota_update_status_message}"
                )

            print("OTA update status:", ota_update_status)

            time.sleep(5)


@main.command(
    name="import", help="Create an Amazon S3 bucket and import update into it"
)
@click.option(
    "--thing-name",
    required=True,
    help="Name of the IoT Thing for which to import the payload",
)
@click.option("--payload-path", required=True, help="Path to a payload to import")
@click.option(
    "--delay",
    default=10,
    show_default=True,
    type=int,
    help="Delay in seconds after importing so that the update becomes deployable",
)
def import_command(thing_name: str, payload_path: str, delay: int) -> None:
    """
    Import an update payload into an Amazon S3 bucket.

    Args:
        thing_name (str): Name of the IoT Thing for which to import the payload.
        payload_path (str): Path to the payload file to import.
        delay (int, optional): Delay in seconds after importing (default is 10 seconds).
    """
    import_update(thing_name, payload_path, delay)


def import_update(thing_name: str, payload_path: str, delay: int = 10) -> None:
    """
    Create an S3 bucket and upload a payload file to it.

    Args:
        thing_name (str): Name of the IoT Thing for which to import the payload.
        payload_path (str): Path to the payload file to upload.
        delay (int, optional): Delay in seconds after uploading (default is 10 seconds).
    """
    s3_bucket_name = PREFIX_S3_BUCKET + thing_name.lstrip(PREFIX_IOT_THING_NAME)

    print(f"Creating S3 bucket `{s3_bucket_name}`")

    try:
        client_s3.create_bucket(
            Bucket=s3_bucket_name,
            ACL="private",
            CreateBucketConfiguration={"LocationConstraint": AWS_REGION},
        )
    except client_s3.exceptions.BucketAlreadyExists:
        print(f"S3 bucket `{s3_bucket_name}` already exists")
    except client_s3.exceptions.BucketAlreadyOwnedByYou:
        print(f"S3 bucket `{s3_bucket_name}` already owned by client")
    else:
        print(f"S3 bucket `{s3_bucket_name}` was created")

    payload = Path(payload_path).name
    with open(payload_path, "rb") as fp:
        try:
            client_s3.put_object(Bucket=s3_bucket_name, Key=payload, Body=fp)
        except Exception as e:
            raise Exception(f"Failed to import payload `{payload}`: {e}")

    time.sleep(delay)

    print(f"`{payload}` was successfully added to S3 bucket `{s3_bucket_name}`")


@main.command(
    name="attach-policy", help="Attach an OTA certificate ARN to the IoT thing"
)
@click.option(
    "--thing-name", required=True, help="Name of the IoT Thing to attach the policy to"
)
def attach_policy_command(thing_name: str) -> None:
    """
    Attach an OTA certificate policy to the IoT thing.

    Args:
        thing_name (str): Name of the IoT Thing to attach the policy to
    """
    attach_policy(thing_name)


def attach_policy(thing_name: str) -> None:
    """
    Attach an OTA certificate policy to the IoT thing.

    Args:
        thing_name (str): Name of the IoT Thing to attach the policy to.
    """
    try:
        client_iot.attach_thing_principal(thingName=thing_name, principal=OTA_POLICY)
    except Exception as e:
        raise Exception(
            f"Failed to attach policy `{OTA_POLICY}` to ioT thing `{thing_name}`: "
            f"{e}"
        )

    print(f"Policy `{OTA_POLICY}` attached to IoT Thing `{thing_name}`")


@main.command(name="deploy", help="Deploy a device update")
@click.option(
    "--thing-name", required=True, help="Name of the IoT Thing to deploy the payload to"
)
@click.option("--payload", required=True, help="Path to a payload to deploy")
@click.option("--signature-path", required=True, help="Path to an update signature")
def deploy_command(thing_name: str, payload: str, signature_path: str) -> None:
    """
    Deploy a device update with a payload and signature.

    Args:
        thing_name (str): Name of the IoT Thing to deploy the payload to
        payload (str): Path to the payload file to deploy.
        signature_path (str): Path to the update signature file.
    """
    deploy_update(thing_name, payload, signature_path)


def deploy_update(thing_name: str, payload: str, signature_path: str) -> None:
    """
    Deploy a device update with a payload and signature.

    Args:
        thing_name (str): Name of the IoT Thing to deploy the payload to.
        payload (str): File name (with extension) of the payload file to deploy.
        signature_path (str): Path to the update signature file.
    """
    s3_bucket_name = PREFIX_S3_BUCKET + thing_name.lstrip(PREFIX_IOT_THING_NAME)
    encoded_signature = bytearray(get_file_content(signature_path).strip(), "utf-8")
    update_files = [
        {
            "fileName": "non_secure image",
            "fileLocation": {
                "s3Location": {
                    "bucket": s3_bucket_name,
                    "key": payload,
                }
            },
            "codeSigning": {
                "customCodeSigning": {
                    "signature": {"inlineDocument": encoded_signature},
                    "certificateChain": {"certificateName": "0"},
                    "hashAlgorithm": "SHA256",
                    "signatureAlgorithm": "RSA",
                }
            },
        }
    ]

    try:
        iot_thing_arn = client_iot.describe_thing(thingName=thing_name)["thingArn"]
    except Exception as e:
        raise Exception(f"Failed to retrieve ARN for ioT thing `{thing_name}`: {e}")

    ota_update_id = PREFIX_OTA_UPDATE_NAME + thing_name.lstrip(PREFIX_IOT_THING_NAME)

    print(f"Creating OTA Update ID `{ota_update_id}`")

    try:
        client_iot.create_ota_update(
            otaUpdateId=ota_update_id,
            targets=[iot_thing_arn],
            protocols=["MQTT"],
            targetSelection="SNAPSHOT",
            files=update_files,
            roleArn=OTA_ROLE_ARN,
        )
    except Exception as e:
        raise Exception(f"Failed to create an IoT OTA update: {e}")

    try:
        wait_for_ota(ota_update_id, "create")
    except Exception as e:
        raise Exception(f"Failed to deploy IoT OTA update: {e}")

    print(f"IoT OTA update `{ota_update_id}` deployed")


@main.command(
    name="cleanup",
    help="Delete the OTA update job, OTA update payload, and S3 bucket",
)
@click.option(
    "--thing-name",
    required=True,
    help="Name of the IoT Thing for which to clean resources",
)
@click.option(
    "--payload",
    required=True,
    help="File name (with extension) of the OTA update payload.",
)
def cleanup_command(thing_name: str, payload: str) -> None:
    """
    Delete resources associated with a deployment.

    Args:
        thing_name (str): Name of the IoT Thing for which to clean resources.
        payload (str): File name (with extension) of the OTA update payload.
    """
    cleanup_resources(thing_name, payload)


def cleanup_resources(thing_name: str, payload: str) -> None:
    """
    Delete resources associated with a deployment.

    Args:
        thing_name (str): Name of the IoT Thing for which to clean resources.
        payload (str): File name (with extension) of the payload to deploy.
    """
    ota_update_id = PREFIX_OTA_UPDATE_NAME + thing_name.lstrip(PREFIX_IOT_THING_NAME)

    try:
        client_iot.delete_ota_update(
            otaUpdateId=ota_update_id, forceDeleteAWSJob=True, deleteStream=True
        )
        wait_for_ota(ota_update_id, "delete")
    except Exception as e:
        print(f"Failed to delete IoT OTA update `{ota_update_id}`: {e}")
    else:
        print(f"OTA Update job `{ota_update_id}` has been deleted")

    s3_bucket_name = PREFIX_S3_BUCKET + thing_name.lstrip(PREFIX_IOT_THING_NAME)

    try:
        client_s3.delete_object(Bucket=s3_bucket_name, Key=payload)
    except Exception as e:
        print(
            f"Failed to delete payload `{payload}` from S3 bucket `{s3_bucket_name}`: "
            f"{e}"
        )
    else:
        print(f"Deleted payload {payload} from S3 bucket {s3_bucket_name}")

    try:
        client_s3.delete_bucket(Bucket=s3_bucket_name)
    except Exception as e:
        print(f"Failed to delete S3 bucket `{s3_bucket_name}`: {e}")
    else:
        print(f"Deleted S3 bucket `{s3_bucket_name}`")


@main.command(
    name="delete-thing",
    help="Delete the IoT thing",
)
@click.option(
    "--thing-name",
    required=True,
    help="Name of the IoT Thing to delete",
)
def delete_thing_command(thing_name: str) -> None:
    """
    Delete an IoT thing.

    Args:
        thing_name (str): Name of the IoT Thing to delete.
    """
    delete_iot_thing(thing_name)


def delete_iot_thing(thing_name: str) -> None:
    """
    Delete an IoT thing

    Args:
        thing_name (str): Name of the IoT Thing to delete.
    """
    try:
        client_iot.detach_thing_principal(thingName=thing_name, principal=OTA_POLICY)
        client_iot.delete_thing(thingName=thing_name)
    except Exception as e:
        print(f"Failed to delete IoT OTA thing `{thing_name}`: {e}")
    else:
        print(f"Deleted IoT thing {thing_name}")


@main.command(
    name="delete-all",
    help=(
        "Delete all AWS resources including OTA update jobs, streams, jobs, "
        "IoT Things, and S3 buckets.",
    ),
)
def delete_all_command():
    delete_all_resources()


def delete_all_resources() -> None:
    """
    Delete all AWS resources including OTA update jobs, streams, jobs, IoT Things,
    and S3 buckets.

    Raises:
        Exception: If any of the resource deletions fail.
    """
    try:
        delete_all_ota_update_jobs()
    except Exception as e:
        raise Exception(f"Failed to delete all OTA update jobs: {e}")

    try:
        delete_all_streams()
    except Exception as e:
        raise Exception(f"Failed to delete all streams: {e}")

    try:
        delete_all_jobs()
    except Exception as e:
        raise Exception(f"Failed to delete all jobs: {e}")

    try:
        delete_all_iot_things()
    except Exception as e:
        raise Exception(f"Failed to delete all IoT Things: {e}")

    try:
        delete_all_buckets()
    except Exception as e:
        raise Exception(f"Failed to delete all S3 buckets: {e}")


def delete_all_ota_update_jobs() -> None:
    """
    Delete all OTA update jobs.

    Raises:
        Exception: If any of the OTA update job deletions fail.
    """
    next_token = ""
    while True:
        response = client_iot.list_ota_updates(nextToken=next_token)

        for ota_update in response["otaUpdates"]:
            update_id = ota_update["otaUpdateId"]
            if update_id.startswith(PREFIX_OTA_UPDATE_NAME):
                try:
                    response = client_iot.get_ota_update(otaUpdateId=update_id)
                    if (
                        response["otaUpdateInfo"]["otaUpdateStatus"]
                        == "DELETE_IN_PROGRESS"
                    ):
                        print(f"Skip OTA job `{update_id}` as deletion is in progress")
                        continue
                    else:
                        client_iot.delete_ota_update(
                            otaUpdateId=update_id,
                            forceDeleteAWSJob=True,
                            deleteStream=True,
                        )
                except Exception as e:
                    raise Exception(
                        f"Failed to delete OTA update job `{update_id}`: {e}"
                    )

                print(f"Deleted {update_id}")

        next_token = response.get("nextToken", "")

        if not next_token:
            break


def delete_all_streams() -> None:
    """
    Delete all IoT streams.

    Raises:
        Exception: If any of the stream deletions fail.
    """
    next_token = ""
    while True:
        response = client_iot.list_streams(nextToken=next_token)

        for stream in response["streams"]:
            stream_id = stream["streamId"]
            if stream_id.startswith(PREFIX_OTA_UPDATE_JOB_NAME):
                try:
                    client_iot.delete_stream(streamId=stream_id)
                except Exception as e:
                    raise Exception(f"Failed to delete stream `{stream_id}`: {e}")

                print(f"Deleted stream {stream_id}")

        next_token = response.get("nextToken", "")

        if not next_token:
            break


def delete_all_jobs() -> None:
    """
    Delete all IoT jobs.

    Raises:
        Exception: If any of the job deletions fail.
    """
    next_token = ""
    while True:
        # Restrict response to 10 results as we cannot process more in one go
        response = client_iot.list_jobs(nextToken=next_token, maxResults=10)

        if response["jobs"] == []:
            return

        for job in response["jobs"]:
            job_id = job["jobId"]
            if job_id.startswith(PREFIX_OTA_UPDATE_JOB_NAME + PREFIX_OTA_UPDATE_NAME):
                if job["status"] == "DELETION_IN_PROGRESS":
                    print(f"Skiping Job {job_id} as deletion is in progress")
                else:
                    try:
                        client_iot.delete_job(jobId=job_id, force=True)
                    except Exception as e:
                        raise Exception(f"Failed to delete job `{job_id}`: {e}")

                print(f"Deleted {job_id}")

        # Delay to delete 10 jobs
        time.sleep(40)

        next_token = response.get("nextToken", "")

        if not next_token:
            break


def delete_all_iot_things() -> None:
    """
    Delete all IoT Things.

    Raises:
        Exception: If any of the IoT Thing deletions fail.
    """
    next_token = ""
    while True:
        response = client_iot.list_things(nextToken=next_token)

        for thing in response["things"]:
            thing_name = thing["thingName"]
            if thing_name.startswith(PREFIX_IOT_THING_NAME):
                try:
                    client_iot.detach_thing_principal(
                        thingName=thing_name, principal=OTA_POLICY
                    )
                    client_iot.delete_thing(thingName=thing_name)
                except Exception as e:
                    raise Exception(f"Failed to IoT thing `{thing_name}`: {e}")

                print(f"Deleted {thing_name}")

        next_token = response.get("nextToken", "")
        if not next_token:
            break


def delete_all_buckets() -> None:
    """
    Delete all S3 buckets.

    Raises:
        Exception: If any of the S3 bucket deletions fail.
    """
    next_token = ""
    while True:
        response = client_s3.list_buckets(nextToken=next_token)

        for bucket in response["Buckets"]:
            bucket_name = bucket["Name"]
            if bucket_name.startswith(PREFIX_S3_BUCKET):
                try:
                    delete_all_bucket_objects(bucket_name)
                except Exception as e:
                    raise Exception(
                        f"Failed to delete all objects from bucket `{bucket_name}`: {e}"
                    )

                print(f"Deleted all objects from {bucket_name}")

                try:
                    client_s3.delete_bucket(Bucket=bucket_name)
                except Exception as e:
                    raise Exception(f"Failed to delete bucket `{bucket_name}`: {e}")

                print(f"Deleted S3 bucket {bucket_name}")

        next_token = response.get("nextToken", "")

        if not next_token:
            break


def delete_all_bucket_objects(bucket_name: str) -> None:
    """
    Delete all S3 bucket objects.

    Raises:
        Exception: If any of the S3 bucket deletions fail.
    """
    try:
        response = client_s3.list_objects_v2(Bucket=bucket_name)

        while True:
            for obj in response.get("Contents", []):
                client_s3.delete_object(Bucket=bucket_name, Key=obj["Key"])

            if not response.get("IsTruncated", False):
                break

            continuation_token = response.get("NextContinuationToken")

            response = client_s3.list_objects_v2(
                Bucket=bucket_name, ContinuationToken=continuation_token
            )
    except Exception as e:
        raise Exception(
            f"Failed to delete all objects from bucket `{bucket_name}`: {e}"
        )


if __name__ == "__main__":
    main()
