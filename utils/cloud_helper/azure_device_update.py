#!/usr/bin/env python3

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

import base64
import click
import hashlib
import os
import time
from datetime import datetime, timedelta, timezone

from azure.core.exceptions import HttpResponseError
from azure.identity import DefaultAzureCredential
from azure.iot.deviceupdate import DeviceUpdateClient
from azure.iot.hub import IoTHubRegistryManager
from azure.storage.blob import ContainerClient

# The following environment variables are predefined in the CI environment and
# same across all pipelines, so they are not passed as command line arguments.
endpoint = os.environ["AZURE_DEVICE_UPDATE_ENDPOINT"]
instance = os.environ["AZURE_DEVICE_UPDATE_INSTANCE"]
storage_url = os.environ["AZURE_STORAGE_URL"]
storage_container = os.environ["AZURE_STORAGE_CONTAINER"]
storage_conn_str = os.environ["AZURE_STORAGE_CONN_STR"]
storage_sas_token = os.environ["AZURE_STORAGE_SAS_TOKEN"]
iothub_conn_str = os.environ["AZURE_IOT_HUB_CONNECTION_STRING"]

# DefaultAzureCredential() gets Azure Active Directory service principal
# from the environment variables
# AZURE_CLIENT_ID, AZURE_CLIENT_SECRET, AZURE_TENANT_ID
device_update_client = DeviceUpdateClient(
    credential=DefaultAzureCredential(), endpoint=endpoint, instance_id=instance
)
storage_client = ContainerClient.from_connection_string(
    storage_conn_str, container_name=storage_container
)
iothub_registry_manager = IoTHubRegistryManager.from_connection_string(iothub_conn_str)


@click.group()
def main():
    pass


def get_file_size(file_path):
    return os.path.getsize(file_path)


def get_file_hash(file_path):
    with open(file_path, "rb") as f:
        bytes = f.read()  # read entire file as bytes
        return base64.b64encode(hashlib.sha256(bytes).digest()).decode("utf-8")


def get_remote_file_path(directory_name, file_name):
    return directory_name + "/" + os.path.basename(file_name)


def get_remote_file_url_with_sas(directory_name, file_name):
    return (
        storage_url
        + "/"
        + storage_container
        + "/"
        + get_remote_file_path(directory_name, file_name)
        + "?"
        + storage_sas_token
    )


@main.command(
    name="import",
    help="Upload files to storage container and import update to Device Update service",
)
@click.option("--manifest", required=True, help="Path to import manifest file")
@click.option(
    "--payload",
    required=True,
    multiple=True,
    help="Path to a payload to import (can be passed more than once)",
)
@click.option("--directory", required=True, help="Directory in the storage container")
@click.option(
    "--delay",
    default=10,
    show_default=True,
    type=int,
    help="Delay in seconds after importing so that the update becomes deployable",
)
def import_command(manifest, payload, directory, delay):
    import_update(manifest, payload, directory, delay)


def import_update(manifest, payloads, directory, delay=10):
    for blob in [manifest, *payloads]:
        with open(blob, "rb") as data:
            remote_path = get_remote_file_path(directory, blob)
            storage_client.upload_blob(name=remote_path, data=data)
            print(f"Uploaded to storage container: {remote_path}")

    update = [
        {
            "importManifest": {
                "url": get_remote_file_url_with_sas(directory, manifest),
                "sizeInBytes": get_file_size(manifest),
                "hashes": {"sha256": get_file_hash(manifest)},
            },
            "files": [],
        }
    ]

    for blob in payloads:
        update[0]["files"].append(
            {
                "fileName": os.path.basename(blob),
                "url": get_remote_file_url_with_sas(directory, blob),
            }
        )

    try:
        response = device_update_client.device_update.begin_import_update(update)
        response.wait()
        time.sleep(delay)
        print("Update imported successfully")
    except HttpResponseError as e:
        raise Exception(f"Failed to import update: {e}")


@main.command(name="deploy", help="Deploy a device update")
@click.option("--deployment-id", required=True, help="ID to give to the deployment")
@click.option("--provider", required=True, help="Provider of the update")
@click.option("--update", required=True, help="Name of the update")
@click.option("--version", required=True, help="Version of the update")
@click.option("--group", required=True, help="Group to deploy the update to")
@click.option(
    "--delay",
    default=0,
    show_default=True,
    type=int,
    help="Delay in seconds so the device has time to get online first",
)
def deploy_command(deployment_id, provider, update, version, group, delay):
    deploy_update(deployment_id, provider, update, version, group, delay)


def deploy_update(deployment_id, provider, update, version, group, delay=0):
    try:
        deployment = {
            "deploymentId": deployment_id,
            "startDateTime": str(datetime.now(timezone.utc) + timedelta(seconds=delay)),
            "update": {
                "updateId": {"provider": provider, "name": update, "version": version},
            },
            "groupId": group,
        }
        device_update_client.device_management.create_or_update_deployment(
            group, deployment_id, deployment
        )
        print(f"Update deployed: {deployment_id}")
    except HttpResponseError as e:
        raise Exception(f"Failed to deploy update: {e}")


def _purge_group(group):
    device_classes = []

    # Note: A group cannot be deleted if it contains any deployments
    try:
        for (
            response
        ) in device_update_client.device_management.list_deployments_for_group(group):
            deploymentId = response["deploymentId"]
            device_update_client.device_management.delete_deployment(
                group, deploymentId
            )
            print(f"Deployment deleted: {deploymentId}")
    except HttpResponseError as e:
        raise Exception(f"Failed to query or delete deployments: {e}")

    # Note: A group cannot be deleted if it contains any devices
    try:
        for response in device_update_client.device_management.list_devices(
            filter=f"groupId eq '{group}'"
        ):
            deviceId = response["deviceId"]
            iothub_registry_manager.delete_device(deviceId)
            print(f"Device deleted: {deviceId}")
            # A device class can only be deleted after the group is deleted, so
            # save it in the list for later deletion.
            device_classes.append(response["deviceClassId"])
    except HttpResponseError as e:
        raise Exception(f"Failed to query or delete devices or device classes: {e}")

    # Wait a bit, so that the group has time to find out that its deployments
    # and devices have been deleted already
    time.sleep(5)

    try:
        device_update_client.device_management.delete_group(group)
        print(f"Group deleted: {group}")
    except HttpResponseError as e:
        raise Exception(f"Failed to query or delete group: {e}")

    # Delete device classes as they are not automatically deleted by Azure
    for deviceClassId in device_classes:
        try:
            device_update_client.device_management.delete_device_class(deviceClassId)
            print(f"Device class deleted: {deviceClassId}")
        except HttpResponseError as e:
            raise Exception(f"Failed to delete device class: {e}")


@main.command(name="cleanup", help="Delete the deployment, update and blobs")
@click.option("--group", required=True, help="Group the updated was deployed to")
@click.option("--provider", required=True, help="Provider of the update")
@click.option("--update", required=True, help="Name of the update")
@click.option("--version", required=True, help="Version of the update")
@click.option(
    "--directory", required=True, help="Directory of blobs in the storage container"
)
def cleanup_command(group, provider, update, version, directory):
    cleanup_resources(group, provider, update, version, directory)


def cleanup_resources(group, provider, update, version, directory):
    # Device groups, updates and blobs can be removed independently of each other.
    # Also, they may not have all been created in the first place if something
    # failed during import and deploy commands. Therefore, one failed removal
    # should not prevent us from continuing to remove the other items, thus we
    # defer any exceptions to the end.
    error = False

    try:
        _purge_group(group)
    except Exception as e:
        print(f"Error: {e}")
        error = True

    try:
        device_update_client.device_update.begin_delete_update(
            provider, update, version
        ).wait()
        print(f"Update deleted: provider={provider}, name={update}, version={version}")
    except HttpResponseError as e:
        print(f"Failed to delete update: {e}")
        error = True

    try:
        for blob in storage_client.list_blobs(name_starts_with=(directory + "/")):
            storage_client.delete_blob(blob.name)
            print(f"Deleted blob from storage container: {blob.name}")
    except HttpResponseError as e:
        print(f"Failed to delete blobs: {e}")
        error = True

    if error:
        raise Exception("Error(s) occurred during cleanup")


@main.command(
    name="delete-all",
    help="Delete all updates, groups and associated devices and deployments",
)
def delete_all_command():
    delete_all_resources()


def delete_all_resources():
    try:
        for response in device_update_client.device_update.list_updates():
            updateId = response["updateId"]
            provider = updateId["provider"]
            name = updateId["name"]
            version = updateId["version"]
            device_update_client.device_update.begin_delete_update(
                provider, name, version
            ).wait()
            print(
                f"Update deleted: provider={provider}, name={name}, version={version}"
            )
    except HttpResponseError as e:
        raise Exception(f"Failed to list or delete updates: {e}")

    try:
        for response in device_update_client.device_management.list_groups():
            _purge_group(response["groupId"])
    except HttpResponseError as e:
        raise Exception(f"Failed to list or purge groups: {e}")

    # In case there are dangling device classes that belong to no groups
    try:
        for response in device_update_client.device_management.list_device_classes():
            deviceClassId = response["deviceClassId"]
            device_update_client.device_management.delete_device_class(deviceClassId)
            print(f"Device class deleted: {deviceClassId}")
    except HttpResponseError as e:
        raise Exception(f"Failed to list or delete device classes: {e}")


if __name__ == "__main__":
    main()
