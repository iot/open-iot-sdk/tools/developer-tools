# v2023.07 (2023-07-18)

## Changes

* ci: Construct repository URL strings without using cut.
* ci: Add the Black Duck scan to the base pipeline
* license: Remove the Matter specific files excluding
* license: Add the yaml and yml files to copyrights checks.


# v2023.04 (2023-04-13)

## Changes

* utils: Create azure_device_update.py for managing Azure Device Update.
* cloud-utils: Update prefix for Azure device ID, as Azure Device Update doesn't work with `_`.
* azure-update: Print import success only if successful.
* azure-update: Raise Exception in case of failure to exit with error.
* azure-update: Fix JSON for importing multiple payloads.
* azure-update: Add a delay so that the imported update becomes deployable.
* azure-update: Improve help text for `--delay`. Python's `click` library already indicates what options are required, so there is no need to say what is optional. Enable `show_default=True` so the user can see the default value.
* azure-update: Add delete-all command to free up all ADU resources, including all updates, device groups, device classes and associated devices and deployments. This is for housekeeping of the ADU instance and must be used with caution, as it can interfere with any ongoing CI runs that involve ADU.
* azure-update: Delete ADU group and device class during cleanup, as Azure has a limit of 100 ADU groups and 80 device classes.
* azure-update: Make functions callable from Python scripts.
* templates: Merge to import using single file.
* license: Exclude Matter specific file from license check.


# v2023.01 (2023-01-15)

## Changes

* Add towncrier news fragments and configuration to the developer-tools.
* towncrier: Fix the path for the template in the pipeline
* gitlint: Allow numbers in commit message prefix, such as `ipv6:`.
* cppcheck: Fix support for C language by not forcing C++, as cppcheck can detect language (C or C++) based on file extension and apply language-specific rules accordingly. Add C99 as a standard.
* public-sync: Sync only when run from internal GitLab instance
* autbot: Fix automerge
* mirror: Pull main changes from public GitLab
* clang-format: Enable sorting includes
* autobot: Update LICENSE.MD file
* ci: Create cloud devices dynamically.
* utils: Add a unique ADUGroup tag for created Azure IoT device.
* release: Automate release
* autobot: Skip MRs worked on by developers


This changelog should be read in conjunction with any release notes provided
for a specific release version.
